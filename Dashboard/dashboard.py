import sys
import pandas as pd
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import QCoreApplication, QDateTime, Qt, QTimer, QDir, QUrl
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
import pyqtgraph as pg

data0 = pd.read_csv("211221_114331_114442.csv")[["distance", "Y/N"]]
data1 = pd.read_csv("distanceResultbila_114442_dashboard.csv")[
    ["distance", "disparity"]
]

temp_list0 = []
temp_list1 = []
temp_list2 = []
temp_list3 = []
warn_list = []
total_list = []
for i in range(len(data0)):
    subtotal_list = []
    temp_list0.append(i)
    temp_list1.append(data0.loc[i].to_list()[0])
    warn_list.append(data0.loc[i].to_list()[1])
    if i < len(data1):
        temp_list2.append(data1.loc[i].to_list()[0])
        temp_list3.append(data1.loc[i].to_list()[1])
    subtotal_list.append(temp_list0.copy())
    subtotal_list.append(temp_list1.copy())
    if i < len(data1):
        subtotal_list.append(temp_list2.copy())
        subtotal_list.append(temp_list3.copy())
    total_list.append(subtotal_list)

w_data = pd.read_csv("amos_data_20181224.csv")[["WD_AVG_10M", "WS_AVG_10M", "QNH"]]
w_list = []
qnh_list = []
for i in range(len(w_data)):
    tempdata = w_data.loc[i].to_list()
    # print(tempdata)
    tempdata[0] = str(int(tempdata[0]))
    if len(tempdata[0]) == 1:
        tempdata[0] = "00" + tempdata[0]
    if len(tempdata[0]) == 2:
        tempdata[0] = "0" + tempdata[0]
    tempdata[1] = "0" + str(int(tempdata[1]))
    w_list.append((tempdata[0][:-1] + "0") + "/" + tempdata[1] + "KT")
    qnh_list.append("Q" + str(int(tempdata[2])))
alt_data = pd.read_csv("distanceResultbila_114442_dashboard.csv")[
    ["Altitude(ft)", "Speed(kt)", "Heading"]
]
alt_list = []
for i in range(len(alt_data)):
    alt_list.append(alt_data.loc[i].to_list())
# print(alt_list)


class MyApp(QWidget):
    def __init__(self):
        super().__init__()
        self.vid_length = 0
        self.mediaPlayer1 = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        fileName, _ = QFileDialog.getOpenFileName(
            self, "video", ".", "Video Files (*.mp4 *.flv *.ts *.mts *.avi *.mov)"
        )
        self.mediaPlayer1.setMedia(QMediaContent(QUrl.fromLocalFile(fileName)))
        videoWidget1 = QVideoWidget()
        self.mediaPlayer1.setVideoOutput(videoWidget1)

        QFontDatabase.addApplicationFont("D2Coding ligature.ttf")
        # big font
        infofont = self.font()
        # infofont.setFamily("NanumGothicCodingLigature")
        infofont.setFamily("D2Coding ligature")
        infofont.setBold(True)
        infofont.setPointSize(100)

        # middle-big font
        windfont = QFont("D2Coding ligature", 75)
        # windfont.setFamily("D2Coding")
        # windfont.setPointSize(75)

        # middle font
        datefont = QFont("D2Coding ligature", 30)
        # datefont.setFamily("D2Coding")
        # datefont.setPointSize(30)
        # small font
        infofont2 = QFont("D2Coding ligature", 20)
        # infofont2.setFamily("D2Coding")
        # infofont2.setPointSize(20)

        # main grid
        grid = QVBoxLayout()
        self.setLayout(grid)

        # top grid
        top_grid = QHBoxLayout()
        # logo grid
        logo_grid = QVBoxLayout()
        logo_rudasys = QPixmap("logo_rudasys2.png").scaledToWidth(200)
        logo_mondrian = QPixmap("logo_mondrian.png").scaledToWidth(200)
        logo_hanseo = QPixmap("logo_hanseo.png").scaledToWidth(200)
        label_rudasys = QLabel()
        label_rudasys.setPixmap(logo_rudasys)
        label_mondrian = QLabel()
        label_mondrian.setPixmap(logo_mondrian)
        label_hanseo = QLabel()
        label_hanseo.setPixmap(logo_hanseo)
        logo_grid.addWidget(label_rudasys)
        logo_grid.addWidget(label_mondrian)
        logo_grid.addWidget(label_hanseo)
        # add logo grid to top grid
        top_grid.addLayout(logo_grid, 1)

        # time and name grid
        time_name_grid = QVBoxLayout()
        # time
        datetime = QLabel(QDateTime.currentDateTime().toString("yyyy/MM/dd"))
        datetime.setAlignment(Qt.AlignCenter)
        datetime.setStyleSheet("color: white;")
        datetime.setFont(datefont)
        time_name_grid.addWidget(datetime)
        # name
        name_grid = QHBoxLayout()
        airlogo = QLabel("     ")
        airlogo.setFont(datefont)
        name_grid.addWidget(airlogo, alignment=Qt.AlignCenter)
        airport = QLabel("RKTA")
        airport.setStyleSheet("color: white;")
        airport.setFont(infofont)
        name_grid.addWidget(airport, alignment=Qt.AlignCenter | Qt.AlignTop)
        airport_2 = QLabel("RWY33")
        airport_2.setFont(datefont)
        airport_2.setStyleSheet("color: white;")
        name_grid.addWidget(airport_2, alignment=Qt.AlignRight)
        time_name_grid.addLayout(name_grid)
        # add time name grid to main grid
        top_grid.addLayout(time_name_grid, 1)

        # info grid
        info_grid = QVBoxLayout()
        wind_grid1 = QHBoxLayout()
        logo_wind1 = QPixmap("logo_wind.png").scaledToWidth(50)
        label_wind1 = QLabel()
        label_wind1.setPixmap(logo_wind1)
        wind_grid1.addWidget(label_wind1, 1, alignment=Qt.AlignRight)
        self.label_info = QLabel(w_list[0])
        self.label_info.setFont(windfont)
        self.label_info.setStyleSheet("color: white;")
        wind_grid1.addWidget(self.label_info, 3, alignment=Qt.AlignCenter)
        info_grid.addLayout(wind_grid1)

        wind_grid2 = QHBoxLayout()
        logo_wind2 = QPixmap("logo_wind2.png").scaledToWidth(50)
        label_wind2 = QLabel()
        label_wind2.setPixmap(logo_wind2)
        wind_grid2.addWidget(label_wind2, 1, alignment=Qt.AlignRight)
        self.wind_label = QLabel("Q1013")
        self.wind_label.setFont(windfont)
        self.wind_label.setStyleSheet("color:white;")
        wind_grid2.addWidget(self.wind_label, 3, alignment=Qt.AlignCenter)
        info_grid.addLayout(wind_grid2)
        # add info grid to main grid
        top_grid.addLayout(info_grid, 1)

        # video grid----------------------------------------------------
        # add video grid to main grid
        video_grid = QHBoxLayout()
        video_grid.addWidget(videoWidget1)

        # control grid----------------------------------------------------
        control_grid = QHBoxLayout()
        self.playButton = QPushButton()
        self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play)
        control_grid.addWidget(self.playButton)
        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setRange(0, 0)
        self.positionSlider.sliderPressed.connect(self.pressSlider)
        self.positionSlider.sliderMoved.connect(self.setPosition)
        self.positionSlider.sliderReleased.connect(self.releaseSlider)
        control_grid.addWidget(self.positionSlider)

        # bottom layout----------------------------------------------------
        bottom_grid = QHBoxLayout()
        # warn_graph_layout
        warn_graph_layout = QVBoxLayout()
        warn_layout = QHBoxLayout()
        self.warn1 = QLabel("warning1")
        self.warn1.setStyleSheet("color: white;")
        self.warn1.setAlignment(Qt.AlignCenter)
        warn2 = QLabel("  ")
        warn2.setStyleSheet("color: white;")
        warn_layout.addWidget(self.warn1)
        warn_layout.addWidget(warn2)
        # graph layout
        graph_layout = QHBoxLayout()
        graph1_layout = QVBoxLayout()
        graph2_layout = QVBoxLayout()
        self.graph1 = pg.PlotWidget()
        self.pen1 = pg.mkPen(width=5)
        self.graph1.getPlotItem().setLabel("bottom", "difference(m)")
        self.graph1.getPlotItem().setLabel("left", "time(s)")
        self.graph1.setXRange(-400, 400, padding=0)
        self.graph2 = pg.PlotWidget()
        self.graph2.setXRange(0, len(data1), padding=0)
        self.graph2.setYRange(-10, 2100, padding=0)
        self.pen2 = pg.mkPen(width=5, style=Qt.DashLine)
        self.graph2.getPlotItem().setLabel("bottom", "time(s)")
        self.graph2.getPlotItem().setLabel("left", "distance(m)")
        graph1_title = QLabel("중심선 이탈편차 분석")
        graph1_title.setFont(infofont2)
        graph1_title.setStyleSheet("color:white;")
        graph1_layout.addWidget(graph1_title, 1, alignment=Qt.AlignCenter)
        graph1_layout.addWidget(self.graph1, 6)
        self.pen3 = pg.mkPen(color="red", style=Qt.DashLine, width=5)
        self.pen_warn = pg.mkPen(color="red", width=5)
        graph2_title = QLabel("훈련기 거리 분석")
        graph2_title.setFont(infofont2)
        graph2_title.setStyleSheet("color:white;")
        graph2_layout.addWidget(graph2_title, 1, alignment=Qt.AlignCenter)
        graph2_layout.addWidget(self.graph2)
        self.graph1.plot(
            total_list[0][1], total_list[0][0], pen=self.pen1
        ).getViewBox().invertY(True)
        self.graph1.plot([0, 0], [0, len(data0)], pen=self.pen3)
        # self.graph1.setTitle(title="중심선 이탈편차 분석", color='white', size='20px')
        self.graph2.plot(total_list[0][0], total_list[0][2], pen=self.pen1)
        # self.graph2.setTitle(title="훈련기 거리 분석", color='white', size='20px')
        graph_layout.addLayout(graph1_layout)
        graph_layout.addLayout(graph2_layout)
        warn_graph_layout.addLayout(graph_layout)

        bottom_grid.addLayout(warn_graph_layout, 6)

        bottom_grid.addWidget(QLabel("      "), 1)
        info_grid_2 = QVBoxLayout()

        info5 = QLabel("Call Sign : HSF1270")
        info5.setStyleSheet("color: white;")
        info5.setFont(infofont2)
        info_grid_2.addWidget(info5)

        info4 = QLabel("Aircraft Type : C172")
        info4.setStyleSheet("color: white;")
        info4.setFont(infofont2)
        info_grid_2.addWidget(info4)

        self.info6 = QLabel("Altitude(FT) : ")
        self.info6.setStyleSheet("color: white;")
        self.info6.setFont(infofont2)
        info_grid_2.addWidget(self.info6)

        self.info7 = QLabel("Speed(KNOT) : ")
        self.info7.setStyleSheet("color: white;")
        self.info7.setFont(infofont2)
        info_grid_2.addWidget(self.info7)

        self.info8 = QLabel("Cource(Degree) : ")
        self.info8.setStyleSheet("color: white;")
        self.info8.setFont(infofont2)
        info_grid_2.addWidget(self.info8)

        self.info2 = QLabel("Distance(m) : ")
        self.info2.setStyleSheet("color: white;")
        self.info2.setFont(infofont2)
        info_grid_2.addWidget(self.info2)

        self.info1 = QLabel("Difference(m) : ")
        self.info1.setStyleSheet("color: white;")
        self.info1.setFont(infofont2)
        info_grid_2.addWidget(self.info1)

        bottom_grid.addLayout(info_grid_2, 2)

        grid.addLayout(top_grid, 4)
        grid.addLayout(video_grid, 8)
        grid.addLayout(control_grid, 1)
        grid.addLayout(bottom_grid, 6)
        grid.addWidget(QLabel("      "), 1)

        # self.statusBar().showMessage('Ready')

        self.setWindowTitle("Dashboard")
        self.setWindowIcon(QIcon("main_logo.png"))
        self.setGeometry(0, 0, 1920, 1080)
        pal = QPalette()
        pal.setColor(QPalette.Background, QColor(0, 0, 15))
        self.setAutoFillBackground(True)
        self.setPalette(pal)

        self.mediaPlayer1.stateChanged.connect(self.mediaStateChanged)
        self.mediaPlayer1.positionChanged.connect(self.positionChanged)
        self.mediaPlayer1.durationChanged.connect(self.durationChanged)
        self.mediaPlayer1.error.connect(self.handleError)

    def play(self):
        if self.mediaPlayer1.state() == QMediaPlayer.PlayingState:
            self.mediaPlayer1.pause()
        else:
            self.mediaPlayer1.play()

    def mediaStateChanged(self, state):
        if self.mediaPlayer1.state() == QMediaPlayer.PlayingState:
            self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPause))
        else:
            self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))

    def positionChanged(self, position):
        self.positionSlider.setValue(position)

        k = int(position / (self.vid_length / len(w_list)) - 1)
        self.label_info.setText(w_list[k])
        self.label_info.repaint()
        self.wind_label.setText(qnh_list[k])
        self.wind_label.repaint()

        self.graph1.clear()
        i = int(position / (self.vid_length / len(data0)) - 1)
        if i < 0:
            i = 0
        if warn_list[i] == 1:
            self.graph1.plot(
                total_list[i][1], total_list[i][0], pen=self.pen_warn
            ).getViewBox().invertY(True)
            sys.stdout.flush()
        else:
            self.graph1.plot(
                total_list[i][1], total_list[i][0], pen=self.pen1
            ).getViewBox().invertY(True)
        self.graph1.plot([0, 0], [0, len(data0)], pen=self.pen3)

        self.info1.setText(
            "Difference(m) : " + str(round(data0.loc[i].to_list()[0], 2))
        )
        self.info1.repaint()
        self.info6.setText("Altitude(FT) : " + str(alt_list[i][0]))
        self.info6.repaint()
        self.info7.setText("Speed(KNOT) : " + str(alt_list[i][1]))
        if int(alt_list[i][1] <= 65):
            self.info7.setStyleSheet("color: green;")
        elif int(alt_list[i][1] > 65) and int(alt_list[i][1] <= 70):
            self.info7.setStyleSheet("color: yellow;")
        elif int(alt_list[i][1] > 70):
            self.info7.setStyleSheet("color: red;")
        self.info7.repaint()
        self.info8.setText("Cource(Degree) : " + str(alt_list[i][2]))
        self.info8.repaint()
        j = int(position / (self.vid_length / len(data1)) - 1)
        if j < 0:
            j = 0
        self.graph2.clear()
        self.graph2.plot(total_list[j][0], total_list[j][2], pen=self.pen1)
        self.info2.setText("Distance(m) : " + str(round(data1.loc[j].to_list()[0], 2)))
        self.info2.repaint()

    def durationChanged(self, duration):
        self.positionSlider.setRange(0, duration)
        self.vid_length = duration

    def setPosition(self, position):
        self.mediaPlayer1.setPosition(position)

    def handleError(self):
        print("Error: " + self.mediaPlayer1.errorString())

    def pressSlider(self):
        if self.mediaPlayer1.state() == QMediaPlayer.PlayingState:
            self.prevStatus = True
            self.mediaPlayer1.pause()

        else:
            self.prevStatus = False

    def releaseSlider(self):
        if self.prevStatus == True:
            self.prevStatus = False
            self.mediaPlayer1.play()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = MyApp()
    ex.show()
    sys.exit(app.exec_())
