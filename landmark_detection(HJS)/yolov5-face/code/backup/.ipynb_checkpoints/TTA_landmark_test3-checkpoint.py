#!/usr/bin/env python
# coding: utf-8

# In[1]:

import time
import matplotlib.pyplot as plt
import os
import cv2 as cv
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from collections import Counter
from PIL import Image
import json
from shapely.geometry import Point, Polygon


# In[2]:


myFile = open('image_xy_label_TTA3.txt', "r")
cnt = 0
while True:
    if myFile.readline()=='':
        break
    cnt += 1
#print(cnt)



# In[3]:


img_label = []

for i in range(0,cnt):
    img_label.append([])
#print(len(img_label))


# In[4]:


import os

path = "./../data/220317_1000_data/TTA_TEST/TEST3/"
file_list = os.listdir(path)
file_list_py = [file for file in file_list if file.endswith(".png")]

#print(len(file_list_py))


# In[5]:


file = open('image_xy_label_TTA3.txt', "r")

while True:
    line = file.readline()
    #print(line)
    img_name  = line.split('.png')[0] +'.png'
    img_name = img_name.split('/')[-1]

    for i in range(0,len(file_list_py)):
        #print(file_list_py[i])
        #print(img_name)
        if img_name == file_list_py[i]:
            #print(img_name)
            img_label[i].append(line)
    if not line:
        break
        
        
 
    #rint(line)

file.close()
#print(img_label)


# In[6]:


filePath = 'image_xy_label_TTA3.txt'
json_filePath = "../data/220317_1000_data/backup/newfile.json"
savePath = '../data/220317_1000_data/result3'
imagePath = path
total = 0
rectcolor = (0, 255, 0)   
img_name =[]
a_list=[]
gh = 0
numberr=-1
img_listlist=[]
for i in range(len(img_label)):
    
    if i+1 > len(file_list_py):
        break
        
        
    #print('---------'*10)
    #print(i)
    
    
    
                
                
    numberr = numberr + 1
    
    #print('number',i+1)
    

    land = []

    
    for j in range(len(img_label[i])):

        #print('item',img_label[i][j].split("/")[4])
        item = img_label[i][j].split("/")[5]
        item2 = item.split(",")
        a = item2[:6] #a:imgname, conf, bounding box
        #print('a',a)
        imgFile = item2[0]
        #print('img_name',imgFile)
       
        
        if imgFile not in img_name:
            img_name.append(imgFile)


# In[7]:



json_filePath = "../data/220317_1000_data/backup/6~52~48.json"
savePath = '../data/220317_1000_data/TTA_TEST/TEST3_save'
imagePath = '../data/220317_1000_data/TTA_TEST/TEST3'
total = 0
rectcolor = (0, 255, 0)   

a_list=[]
gh = 0
numberr=-1
img_listlist=[]
for i in range(len(img_label)):
    
    if i + 1> len(file_list_py):
        break
        

    print('---------'*10)

    print('파일 개수',i+1)
    print('파일 이름',img_name[i])
      
    numberr = numberr + 1
    
    
    

    land = []

    
    for j in range(len(img_label[i])):
        #print(img_label[i][j].split("/"))
        #print('item',img_label[i][j].split("/")[5])
        item = img_label[i][j].split("/")[5]
        item2 = item.split(",")
        a = item2[:6] #a:imgname, conf, bounding box
        #print('a',a)
        imgFile = item2[0]
        #print('img_name',imgFile)
       
        
        if imgFile not in img_name:
            img_name.append(imgFile)
        
        
        b = item2[6:]
        b[0] = b[0].split("[")[1]
        #print(b[0])
        b[9] = b[9].split("]")[0] 

        p = len(land)
        if p > 0:
            if(land[-1][0]<b[0]):
                land.append(b)                     
            else:
                if(land[0][0]<b[0]):
                    land.insert(1, b)
                else:
                    land.insert(0, b)
        else:
            land.append(b)
        
    
        x1= int(a[2]) #bbox x좌표(min)
        x2= int(a[4]) #bbox x2좌표(max)
        y1= int(a[3]) #bbox x좌표(min)
        y2= int(a[5])

        real_bbox_list =[]
        bbox_w = int(x2-x1)
        bbox_h = int(y2-y1)
        bbox_g = int(bbox_w)*0.02
        #print('bbox_g',bbox_g)
        bbox_list=[]
        x_list =[]
        
        for j in range(len(img_label[i])):
            
            item = img_label[i][j].split("/")[5]
            #print(item)
            item2 = item.split(",")
            
            a = item2[:6] #a:imgname, conf, bounding box

            x1= int(a[2]) #bbox x좌표(min)
            x2= int(a[4]) #bbox x2좌표(max)

            bbox_w = int(x2-x1)
            bbox_g = int(bbox_w)*0.02
        
            x_list.append(x1)
            bbox_list.append(bbox_g)
#         print('sssssssx_list',x_list)
#         print('sssssssbbox_list',bbox_list)

            
            
        with open(json_filePath, "r") as json_file:
            json_data = json.load(json_file)
            #print(json_data[i]['Label']['objects'])
            
            
    
            for ii in range (0,(len(json_data))):
                
                aa = (json_data[ii]['External ID'])

                lw_list_x = []
                lw_list_y = []

                rw_list_x = []
                rw_list_y = []

                ct_list_x =[]
                ct_list_y =[]

                lg_list_x = []
                lg_list_y = []

                rg_list_x = []
                rg_list_y = []

                x1= int(a[2]) #bbox x좌표(min)
                x2= int(a[4]) #bbox x2좌표(max)
                y1= int(a[3]) #bbox x좌표(min)
                y2= int(a[5])

                bbox_w = int(x2-x1)
                bbox_h = int(y2-y1)
                bbox_g = int(bbox_w)*0.02
                

                if aa in imgFile:
                    #print(img)
                    #print('ii',ii)
                    img = cv.imread(f"{imagePath}/{aa}")
             
                    for i3 in json_data[ii]['Label']['objects']:
                        if i3['title'] == 'Left wing':
                            lw_x = i3['point']['x']
                            lw_y = i3['point']['y']  
                            
                            lwx_1 = lw_x - bbox_g
                            lwy_1 = lw_y + bbox_g
                            lwx_2 = lw_x + bbox_g
                            lwy_2 = lw_y - bbox_g

                        
                            lw_list_x.append(lw_x)
                            lw_list_y.append(lw_y)


                        if i3['title'] == 'Right wing':
                            rw_x = i3['point']['x']
                            rw_y = i3['point']['y'] 
                            
                            rwx_1 = rw_x - bbox_g
                            rwy_1 = rw_y + bbox_g
                            rwx_2 = rw_x + bbox_g
                            rwy_2 = rw_y - bbox_g

                 
                            rw_list_x.append(rw_x)
                            rw_list_y.append(rw_y)
         
      
                        if i3['title']  == 'Propeller':
                            ct_x = i3['point']['x']
                            ct_y = i3['point']['y']
                    
                    
                    
                            ctx_1 = ct_x - bbox_g
                            cty_1 = ct_y + bbox_g
                            ctx_2 = ct_x + bbox_g
                            cty_2 = ct_y - bbox_g

                            
                            ct_list_x.append(ct_x)
                            ct_list_y.append(ct_y)
  

                        if i3['title']  == 'Left gear':
                            lg_x = i3['point']['x']
                            lg_y = i3['point']['y']
                    
                            lgx_1 = lg_x - bbox_g
                            lgy_1 = lg_y + bbox_g
                            lgx_2 = lg_x + bbox_g
                            lgy_2 = lg_y - bbox_g

                            lg_list_x.append(lg_x)
                            lg_list_y.append(lg_y)

                        if i3['title']  == 'Right gear':
                            rg_x = i3['point']['x']
                            rg_y = i3['point']['y']
                            
                            rgx_1 = rg_x - bbox_g
                            rgy_1 = rg_y + bbox_g
                            rgx_2 = rg_x + bbox_g
                            rgy_2 = rg_y - bbox_g

                            rg_list_x.append(rg_x)
                            rg_list_y.append(rg_y)

                        
                    lw_list_x_num = np.array(lw_list_x)
                    index = lw_list_x_num.argsort()
                    

                    real_lw_list_x = []
                    real_lw_list_y = []

                    real_rw_list_x = []
                    real_rw_list_y = []

                    real_ct_list_x =[]
                    real_ct_list_y =[]

                    real_lg_list_x = []
                    real_lg_list_y = []

                    real_rg_list_x = []
                    real_rg_list_y = []
                    
                    
                    x_list2 = np.array(x_list)
                    index_x = x_list2.argsort()
                    #print('x_list2',x_list2)
                    #print('index_x',index_x)
                    for nm in index_x:
                        real_bbox_list.append(bbox_list[nm])

                    for nm in index:
                       
                        real_lw_list_x.append(lw_list_x[nm])
                        real_lw_list_y.append(lw_list_y[nm])
                        
                        real_rw_list_x.append(rw_list_x[nm])
                        real_rw_list_y.append(rw_list_y[nm])
                        
                        real_ct_list_x.append(ct_list_x[nm])
                        real_ct_list_y.append(ct_list_y[nm])
                        
                        real_lg_list_x.append(lg_list_x[nm])
                        real_lg_list_y.append(lg_list_y[nm])
                        
                        real_rg_list_x.append(rg_list_x[nm])
                        real_rg_list_y.append(rg_list_y[nm])
                    if len(land) == len(img_label[i]) == int(len(json_data[ii]['Label']['objects'])/6):

                        n = 0 
                        answer =[]
                        for hh in range(0, (len(img_label[i]))):


                            #print('left wing')
                            
                            lwx_1 = real_lw_list_x[hh]
                            lwy_1 = real_lw_list_y[hh]
                            bbox_g = real_bbox_list[hh]
                            #print('bbox_g',real_bbox_list)
                            
                            if int(lwx_1-bbox_g) <= int(land[hh][0]) <= int(lwx_1+bbox_g):
                                if  int(lwy_1-bbox_g) <= int(land[hh][1]) <= int(lwy_1+bbox_g) :
                                    #print('RELALAL True')
                                    A  ='True'
                                    n = n +1
                                    
                                else: 
                                    #print('REAl FALSE')
                                    A  ='False'
           
                            else: 
                                    #print('REAl FALSE')
                                    A  ='False'
                                    
                            answer.append(A)  
                            #print('right wing')
                            
                            rwx_1 = real_rw_list_x[hh]
                            rwy_1 = real_rw_list_y[hh]

                            engineer = [(int(rwx_1-bbox_g)+1, int(rwy_1+bbox_g)+1),(int(rwx_1-bbox_g)+1, int(rwy_1-bbox_g)+1),(int(rwx_1+bbox_g)+1, int(rwy_1-bbox_g)+1),(int(rwx_1+bbox_g)+1, int(rwy_1+bbox_g)+1)]
                            engineer_poly =  Polygon(shell= engineer)
                            
                            test_code1 = Point(int(land[hh][4]), int(land[hh][5]))
                            #print('right wing',test_code1.within(engineer_poly))
                            
                            if int(rwx_1-bbox_g) <= int(land[hh][4]) <= int(rwx_1+bbox_g):
                                if  int(rwy_1-bbox_g) <= int(land[hh][5]) <= int(rwy_1+bbox_g) :
                                    A  ='True'
                                    n = n +1
                                    
                                else: 
                                    #print('REAl FALSE')
                                    A  ='False'
                            else: 
                                    #print('REAl FALSE')
                                    A  ='False'
                                    
                            answer.append(A)       
                           
                            
                            #print('center')
                            ctx_1 = real_ct_list_x[hh]
                            cty_1 = real_ct_list_y[hh]

                            engineer = [(int(ctx_1-bbox_g)+1, int(cty_1+bbox_g)+1),(int(ctx_1-bbox_g)+1, int(cty_1-bbox_g)+1),(int(ctx_1+bbox_g)+1, int(cty_1-bbox_g)+1),(int(ctx_1+bbox_g)+1, int(cty_1+bbox_g)+1)]
                            engineer_poly =  Polygon(shell=engineer)
                            
                            if int(ctx_1-bbox_g) <= int(land[hh][8]) <= int(ctx_1+bbox_g):
                                if  int(cty_1-bbox_g) <= int(land[hh][9]) <= int(cty_1+bbox_g) :
                                    #print('RELALAL True')
                                    A  ='True'
                                    n = n +1
                                    
                                else: 
                                    #print('REAl FALSE')
                                    A  ='False'
                            else: 
                                    #print('REAl FALSE')
                                    A  ='False'
                            

                            answer.append(A)
                            
                            #print('left gear')
                            
                            lgx_1 = real_lg_list_x[hh]
                            lgy_1 = real_lg_list_y[hh]
                            
                            if int(lgx_1-bbox_g) <= int(land[hh][2]) <= int(lgx_1+bbox_g):
                                if  int(lgy_1-bbox_g) <= int(land[hh][3]) <= int(lgy_1+bbox_g) :
                                    #print('RELALAL True')
                                    A  ='True'
                                    n = n +1
                                    
                                else: 
                                    #print('REAl FALSE')
                                    A  ='False'
                            else: 
                                    #print('REAl FALSE')
                                    A  ='False'
                                    
                                    
                            answer.append(A)
                            #print('right gear')
                            
                            rgx_1 = real_rg_list_x[hh]
                            rgy_1 = real_rg_list_y[hh]

                            
                            engineer = [(int(rgx_1-bbox_g)+1, int(rgy_1+bbox_g)+1),(int(rgx_1-bbox_g)+1, int(rgy_1-bbox_g)+1),(int(rgx_1+bbox_g)+1, int(rgy_1-bbox_g)+1),(int(rgx_1+bbox_g)+1, int(rgy_1+bbox_g)+1)]
                            engineer_poly =  Polygon(shell=engineer)
                            
                            test_code1 = Point(int(land[hh][6]), int(land[hh][7]))
                            #print('right_wing',test_code1.within(engineer_poly))
                            
                            if int(rgx_1-bbox_g) <= int(land[hh][6]) <= int(rgx_1+bbox_g):
                                if  int(rgy_1-bbox_g) <= int(land[hh][7]) <= int(rgy_1+bbox_g) :
                                    A  ='True'
                                    n = n +1
                                    #print('RELAL True')
                                    
                                else: 
                                    #print('REAl FALSE')
                                    A  ='False'
                            else: 
                                    #print('REAl FALSE')
                                    A  ='False'
                                    
                                    
                            answer.append(A)

                            cv.rectangle(img,(int(rgx_1-bbox_g), int(rgy_1-bbox_g)), (int(rgx_1+bbox_g), int(rgy_1+bbox_g)), rectcolor, 1)
                            cv.rectangle(img,(int(lgx_1-bbox_g), int(lgy_1-bbox_g)), (int(lgx_1+bbox_g), int(lgy_1+bbox_g)), rectcolor, 1)
                            cv.rectangle(img,(int(ctx_1-bbox_g), int(cty_1-bbox_g)), (int(ctx_1+bbox_g), int(cty_1+bbox_g)), rectcolor, 1)
                            cv.rectangle(img,(int(rwx_1-bbox_g), int(rwy_1-bbox_g)), (int(rwx_1+bbox_g), int(rwy_1+bbox_g)), rectcolor, 1)
                            cv.rectangle(img,(int(lwx_1-bbox_g), int(lwy_1-bbox_g)), (int(lwx_1+bbox_g), int(lwy_1+bbox_g)), rectcolor, 1)
                            
                            
                            cv.circle(img,(int(land[hh][0]), int(land[hh][1])), 1,(255,0,0),-1)
                            cv.circle(img,(int(land[hh][2]), int(land[hh][3])), 1,(255,0,0),-1)
                            cv.circle(img,(int(land[hh][4]), int(land[hh][5])), 1,(255,0,0),-1)
                            cv.circle(img,(int(land[hh][6]), int(land[hh][7])), 1,(255,0,0),-1)
                            cv.circle(img,(int(land[hh][8]), int(land[hh][9])), 1,(255,0,0),-1)

                            cv.circle(img,(int(lwx_1), int(lwy_1)), 1,(0,0,255),-1)
                            cv.circle(img,(int(rwx_1), int(rwy_1)), 1,(0,0,255),-1)
                            cv.circle(img,(int(ctx_1), int(cty_1)), 1,(0,0,255),-1)
                            cv.circle(img,(int(lgx_1), int(lgy_1)), 1,(0,0,255),-1)
                            cv.circle(img,(int(rgx_1), int(rgy_1)), 1,(0,0,255),-1)

                       
                        cv.imwrite(f"{savePath}/{aa}", img)
                        True_list=[]
                        True_score =[]
                        for y in range (0,len(land)):
                            
                            print(answer[5*(y):5*(y+1)])
                            T =0
                            for ijij in answer[5*(y):5*(y+1)]:
                                if ijij  == 'True':
                                    T = T+1
                                
                            True_score.append(T/5*100)
                            
                            print('각 이미지의 정확도',True_score)
                            
                            
                            real_answer =  sum(True_score)/len(land)
                            
                            
                        print('이미지의 평균 정확도',real_answer)
                        #print('Evaluation', n/len(answer)*100)
                            
                    
                        #print('aa',aa)
                        #print(i)
                        #print('img_listlist.',img_listlist)
                        if  i not in img_listlist :
                            if (real_answer) >= 80:
                                img_listlist.append(numberr)
                                gh = gh+1
                                
                        print('정확도 80%이상 개수',gh)
                        time.sleep(1)
                        
                    

                    
print('----------------최종 결과 ---------------------')
print('평균 결과', gh/len(file_list_py)*100)                    


# In[ ]:





# In[ ]:





# In[ ]:





# In[66]:


# filePath = 'image_xy_label_20220324.txt'
# json_filePath = "../data/220317_1000_data/backup/6~52~48.json"
# savePath = '../data/220317_1000_data/result3'
# imagePath = '../data/220317_1000_data/test'
# total = 0
# rectcolor = (0, 255, 0)   

# a_list=[]
# gh = 0
# numberr=-1
# img_listlist=[]
# for i in range(len(img_label)):
    
#     if i+1 > len(file_list_py):
#         break
        
        
#     print('---------'*10)
#     print('img_name',img_name[i])
      
#     numberr = numberr + 1
    
#     print('number',i+1)
    

#     land = []

    
#     for j in range(len(img_label[i])):

#         #print('item',img_label[i][j].split("/")[4])
#         item = img_label[i][j].split("/")[4]
#         item2 = item.split(",")
#         a = item2[:6] #a:imgname, conf, bounding box
#         #print('a',a)
#         imgFile = item2[0]
#         #print('img_name',imgFile)
       
        
#         if imgFile not in img_name:
#             img_name.append(imgFile)
        
        
#         b = item2[6:]
#         b[0] = b[0].split("[")[1]
#         #print(b[0])
#         b[9] = b[9].split("]")[0] 

#         p = len(land)
#         if p > 0:
#             if(land[-1][0]<b[0]):
#                 land.append(b)                     
#             else:
#                 if(land[0][0]<b[0]):
#                     land.insert(1, b)
#                 else:
#                     land.insert(0, b)
#         else:
#             land.append(b)
        
    
#         x1= int(a[2]) #bbox x좌표(min)
#         x2= int(a[4]) #bbox x2좌표(max)
#         y1= int(a[3]) #bbox x좌표(min)
#         y2= int(a[5])

#         real_bbox_list =[]
#         bbox_w = int(x2-x1)
#         bbox_h = int(y2-y1)
#         bbox_g = int(bbox_w)*0.02
#         #print('bbox_g',bbox_g)
#         bbox_list=[]
#         x_list =[]
        
#         for j in range(len(img_label[i])):

#             a = item2[:6] #a:imgname, conf, bounding box

#             x1= int(a[2]) #bbox x좌표(min)
#             x2= int(a[4]) #bbox x2좌표(max)

#             bbox_w = int(x2-x1)
#             bbox_g = int(bbox_w)*0.02
        
#             x_list.append(x1)
#             bbox_list.append(bbox_g)


#         with open(json_filePath, "r") as json_file:
#             json_data = json.load(json_file)
#             #print(json_data[i]['Label']['objects'])
            
            
    
#             for ii in range (0,(len(json_data))):
                
#                 aa = (json_data[ii]['External ID'])

#                 lw_list_x = []
#                 lw_list_y = []

#                 rw_list_x = []
#                 rw_list_y = []

#                 ct_list_x =[]
#                 ct_list_y =[]

#                 lg_list_x = []
#                 lg_list_y = []

#                 rg_list_x = []
#                 rg_list_y = []

#                 x1= int(a[2]) #bbox x좌표(min)
#                 x2= int(a[4]) #bbox x2좌표(max)
#                 y1= int(a[3]) #bbox x좌표(min)
#                 y2= int(a[5])

#                 bbox_w = int(x2-x1)
#                 bbox_h = int(y2-y1)
#                 bbox_g = int(bbox_w)*0.02
                

#                 if aa in imgFile:
#                     #print(img)
#                     #print('ii',ii)
#                     img = cv.imread(f"{imagePath}/{aa}")
             
#                     for i3 in json_data[ii]['Label']['objects']:
#                         if i3['title'] == 'Left wing':
#                             lw_x = i3['point']['x']
#                             lw_y = i3['point']['y']  
                            
#                             lwx_1 = lw_x - bbox_g
#                             lwy_1 = lw_y + bbox_g
#                             lwx_2 = lw_x + bbox_g
#                             lwy_2 = lw_y - bbox_g

                        
#                             lw_list_x.append(lw_x)
#                             lw_list_y.append(lw_y)


#                         if i3['title'] == 'Right wing':
#                             rw_x = i3['point']['x']
#                             rw_y = i3['point']['y'] 
                            
#                             rwx_1 = rw_x - bbox_g
#                             rwy_1 = rw_y + bbox_g
#                             rwx_2 = rw_x + bbox_g
#                             rwy_2 = rw_y - bbox_g

                 
#                             rw_list_x.append(rw_x)
#                             rw_list_y.append(rw_y)
         
      
#                         if i3['title']  == 'Propeller':
#                             ct_x = i3['point']['x']
#                             ct_y = i3['point']['y']
                    
                    
                    
#                             ctx_1 = ct_x - bbox_g
#                             cty_1 = ct_y + bbox_g
#                             ctx_2 = ct_x + bbox_g
#                             cty_2 = ct_y - bbox_g

                            
#                             ct_list_x.append(ct_x)
#                             ct_list_y.append(ct_y)
  

#                         if i3['title']  == 'Left gear':
#                             lg_x = i3['point']['x']
#                             lg_y = i3['point']['y']
                    
#                             lgx_1 = lg_x - bbox_g
#                             lgy_1 = lg_y + bbox_g
#                             lgx_2 = lg_x + bbox_g
#                             lgy_2 = lg_y - bbox_g

#                             lg_list_x.append(lg_x)
#                             lg_list_y.append(lg_y)

#                         if i3['title']  == 'Right gear':
#                             rg_x = i3['point']['x']
#                             rg_y = i3['point']['y']
                            
#                             rgx_1 = rg_x - bbox_g
#                             rgy_1 = rg_y + bbox_g
#                             rgx_2 = rg_x + bbox_g
#                             rgy_2 = rg_y - bbox_g

#                             rg_list_x.append(rg_x)
#                             rg_list_y.append(rg_y)

                        
#                     lw_list_x_num = np.array(lw_list_x)
#                     index = lw_list_x_num.argsort()
                    

#                     real_lw_list_x = []
#                     real_lw_list_y = []

#                     real_rw_list_x = []
#                     real_rw_list_y = []

#                     real_ct_list_x =[]
#                     real_ct_list_y =[]

#                     real_lg_list_x = []
#                     real_lg_list_y = []

#                     real_rg_list_x = []
#                     real_rg_list_y = []
                    
                    
#                     x_list2 = np.array(x_list)
#                     index_x = x_list2.argsort()

                    
#                     for nm in index_x:
#                         real_bbox_list.append(bbox_list[nm])

#                     for nm in index:
                       
#                         real_lw_list_x.append(lw_list_x[nm])
#                         real_lw_list_y.append(lw_list_y[nm])
                        
#                         real_rw_list_x.append(rw_list_x[nm])
#                         real_rw_list_y.append(rw_list_y[nm])
                        
#                         real_ct_list_x.append(ct_list_x[nm])
#                         real_ct_list_y.append(ct_list_y[nm])
                        
#                         real_lg_list_x.append(lg_list_x[nm])
#                         real_lg_list_y.append(lg_list_y[nm])
                        
#                         real_rg_list_x.append(rg_list_x[nm])
#                         real_rg_list_y.append(rg_list_y[nm])
#                     if len(land) == len(img_label[i]) == int(len(json_data[ii]['Label']['objects'])/6):

#                         n = 0 
#                         answer =[]
#                         for hh in range(0, (len(img_label[i]))):


#                             #print('left wing')
                            
#                             lwx_1 = real_lw_list_x[hh]
#                             lwy_1 = real_lw_list_y[hh]
#                             bbox_g = real_bbox_list[hh]

                            
#                             if int(lwx_1-bbox_g) <= int(land[hh][0]) <= int(lwx_1+bbox_g):
#                                 if  int(lwy_1-bbox_g) <= int(land[hh][1]) <= int(lwy_1+bbox_g) :
#                                     #print('RELALAL True')
#                                     A  ='True'
#                                     n = n +1
                                    
#                                 else: 
#                                     #print('REAl FALSE')
#                                     A  ='False'
           
#                             else: 
#                                     #print('REAl FALSE')
#                                     A  ='False'
                                    
#                             answer.append(A)  
#                             #print('right wing')
                            
#                             rwx_1 = real_rw_list_x[hh]
#                             rwy_1 = real_rw_list_y[hh]

#                             engineer = [(int(rwx_1-bbox_g)+1, int(rwy_1+bbox_g)+1),(int(rwx_1-bbox_g)+1, int(rwy_1-bbox_g)+1),(int(rwx_1+bbox_g)+1, int(rwy_1-bbox_g)+1),(int(rwx_1+bbox_g)+1, int(rwy_1+bbox_g)+1)]
#                             engineer_poly =  Polygon(shell= engineer)
                            
#                             test_code1 = Point(int(land[hh][4]), int(land[hh][5]))
#                             #print('right wing',test_code1.within(engineer_poly))
                            
#                             if int(rwx_1-bbox_g) <= int(land[hh][4]) <= int(rwx_1+bbox_g):
#                                 if  int(rwy_1-bbox_g) <= int(land[hh][5]) <= int(rwy_1+bbox_g) :
#                                     A  ='True'
#                                     n = n +1
                                    
#                                 else: 
#                                     #print('REAl FALSE')
#                                     A  ='False'
#                             else: 
#                                     #print('REAl FALSE')
#                                     A  ='False'
                                    
#                             answer.append(A)       
                           
                            
#                             #print('center')
#                             ctx_1 = real_ct_list_x[hh]
#                             cty_1 = real_ct_list_y[hh]

#                             engineer = [(int(ctx_1-bbox_g)+1, int(cty_1+bbox_g)+1),(int(ctx_1-bbox_g)+1, int(cty_1-bbox_g)+1),(int(ctx_1+bbox_g)+1, int(cty_1-bbox_g)+1),(int(ctx_1+bbox_g)+1, int(cty_1+bbox_g)+1)]
#                             engineer_poly =  Polygon(shell=engineer)
                            
#                             if int(ctx_1-bbox_g) <= int(land[hh][8]) <= int(ctx_1+bbox_g):
#                                 if  int(cty_1-bbox_g) <= int(land[hh][9]) <= int(cty_1+bbox_g) :
#                                     #print('RELALAL True')
#                                     A  ='True'
#                                     n = n +1
                                    
#                                 else: 
#                                     #print('REAl FALSE')
#                                     A  ='False'
#                             else: 
#                                     #print('REAl FALSE')
#                                     A  ='False'
                            

#                             answer.append(A)
                            
#                             #print('left gear')
                            
#                             lgx_1 = real_lg_list_x[hh]
#                             lgy_1 = real_lg_list_y[hh]
                            
#                             if int(lgx_1-bbox_g) <= int(land[hh][2]) <= int(lgx_1+bbox_g):
#                                 if  int(lgy_1-bbox_g) <= int(land[hh][3]) <= int(lgy_1+bbox_g) :
#                                     #print('RELALAL True')
#                                     A  ='True'
#                                     n = n +1
                                    
#                                 else: 
#                                     #print('REAl FALSE')
#                                     A  ='False'
#                             else: 
#                                     #print('REAl FALSE')
#                                     A  ='False'
                                    
                                    
#                             answer.append(A)
#                             #print('right gear')
                            
#                             rgx_1 = real_rg_list_x[hh]
#                             rgy_1 = real_rg_list_y[hh]

                            
#                             engineer = [(int(rgx_1-bbox_g)+1, int(rgy_1+bbox_g)+1),(int(rgx_1-bbox_g)+1, int(rgy_1-bbox_g)+1),(int(rgx_1+bbox_g)+1, int(rgy_1-bbox_g)+1),(int(rgx_1+bbox_g)+1, int(rgy_1+bbox_g)+1)]
#                             engineer_poly =  Polygon(shell=engineer)
                            
#                             test_code1 = Point(int(land[hh][6]), int(land[hh][7]))
#                             #print('right_wing',test_code1.within(engineer_poly))
                            
#                             if int(rgx_1-bbox_g) <= int(land[hh][6]) <= int(rgx_1+bbox_g):
#                                 if  int(rgy_1-bbox_g) <= int(land[hh][7]) <= int(rgy_1+bbox_g) :
#                                     A  ='True'
#                                     n = n +1
#                                     #print('RELAL True')
                                    
#                                 else: 
#                                     #print('REAl FALSE')
#                                     A  ='False'
#                             else: 
#                                     #print('REAl FALSE')
#                                     A  ='False'
                                    
                                    
#                             answer.append(A)

#                             cv.rectangle(img,(int(rgx_1-bbox_g), int(rgy_1-bbox_g)), (int(rgx_1+bbox_g), int(rgy_1+bbox_g)), rectcolor, 1)
#                             cv.rectangle(img,(int(lgx_1-bbox_g), int(lgy_1-bbox_g)), (int(lgx_1+bbox_g), int(lgy_1+bbox_g)), rectcolor, 1)
#                             cv.rectangle(img,(int(ctx_1-bbox_g), int(cty_1-bbox_g)), (int(ctx_1+bbox_g), int(cty_1+bbox_g)), rectcolor, 1)
#                             cv.rectangle(img,(int(rwx_1-bbox_g), int(rwy_1-bbox_g)), (int(rwx_1+bbox_g), int(rwy_1+bbox_g)), rectcolor, 1)
#                             cv.rectangle(img,(int(lwx_1-bbox_g), int(lwy_1-bbox_g)), (int(lwx_1+bbox_g), int(lwy_1+bbox_g)), rectcolor, 1)
                            
                            
#                             cv.circle(img,(int(land[hh][0]), int(land[hh][1])), 1,(255,0,0),-1)
#                             cv.circle(img,(int(land[hh][2]), int(land[hh][3])), 1,(255,0,0),-1)
#                             cv.circle(img,(int(land[hh][4]), int(land[hh][5])), 1,(255,0,0),-1)
#                             cv.circle(img,(int(land[hh][6]), int(land[hh][7])), 1,(255,0,0),-1)
#                             cv.circle(img,(int(land[hh][8]), int(land[hh][9])), 1,(255,0,0),-1)

#                             cv.circle(img,(int(lwx_1), int(lwy_1)), 1,(0,0,255),-1)
#                             cv.circle(img,(int(rwx_1), int(rwy_1)), 1,(0,0,255),-1)
#                             cv.circle(img,(int(ctx_1), int(cty_1)), 1,(0,0,255),-1)
#                             cv.circle(img,(int(lgx_1), int(lgy_1)), 1,(0,0,255),-1)
#                             cv.circle(img,(int(rgx_1), int(rgy_1)), 1,(0,0,255),-1)

                       
#                         cv.imwrite(f"{savePath}/{aa}", img)
#                         print('answer',answer)
#                         print('Evaluation', n/len(answer)*100)
                            
                    
#                         #print('aa',aa)
#                         print(i)
#                         #print('img_listlist.',img_listlist)
#                         if  i not in img_listlist :
#                             if (n/len(answer)*100) >= 80:
#                                 img_listlist.append(numberr)
#                                 gh = gh+1

#                     print('gh',gh)
#                     print('평균 결과', gh/len(file_list_py))

                   


# In[9]:



# with open(json_filePath, "r") as json_file:
#     json_data = json.load(json_file)
#     #print(json_data[i]['Label']['objects'])
#     for ii in range (0,(len(json_data))):
#         aa = (json_data[ii]['External ID'])

    
#         a_list_x1 = []
#         a_list_y1 = []
#         a_list_x2 = []
#         a_list_y2 = []

#         lw_list_x = []
#         lw_list_y = []

#         rw_list_x = []
#         rw_list_y = []

#         ct_list_x =[]
#         ct_list_y =[]

#         lg_list_x = []
#         lg_list_y = []

#         rg_list_x = []
#         rg_list_y = []


#         if aa in imgFile:
#             print(aa)
#             for i3 in json_data[ii]['Label']['objects']:
#                 if i3['title'] == 'Left wing':
#                     lw_x = i3['point']['x']
#                     lw_y = i3['point']['y']  

#                     #print('lw_x',lw_x)
# #                         lwx_1 = lw_x - bbox_g
# #                         lwy_1 = lw_y + bbox_g
# #                         lwx_2 = lw_x + bbox_g
# #                         lwy_2 = lw_y - bbox_g

#                     lw_list_x.append(lw_x)
#                     lw_list_y.append(lw_y)


#                 #print('lw_list_x',lw_list_x)
#                 lw_list_x_num = np.array(lw_list_x)
#                 #print('index',lw_list_x_num.argsort())
#                 lw_list_x.sort()

#                 if i3['title'] == 'Right wing':
#                     rw_x = i3['point']['x']
#                     rw_y = i3['point']['y'] 

#                     #print('rw_x',rw_x)

#                     rw_list_x.append(rw_x)
#                     rw_list_y.append(rw_y)
#                 #print('rw_list_x',rw_list_x)
            
#                 if i3['title']  == 'Propeller':
#                     ct_x = i3['point']['x']
#                     ct_y = i3['point']['y']
#                     ct_list_x.append(ct_x)
#                     ct_list_y.append(ct_y)
#                 #print('ct_list_x',ct_list_x)
            
#                 if i3['title']  == 'Left gear':
#                     lg_x = i3['point']['x']
#                     lg_y = i3['point']['y']
#                     lg_list_x.append(lg_x)
#                     lg_list_y.append(lg_y)
#                 #print('lg_list_x',lg_list_x)
            
            
#                 if i3['title']  == 'Right gear':
#                     rg_x = i3['point']['x']
#                     rg_y = i3['point']['y']
#                     rg_list_x.append(rg_x)
#                     rg_list_y.append(rg_y)
#                 #print('rg_list_x',rg_list_x)  
                
                
#             print('index',lw_list_x_num.argsort())
#             print('lw_list_x',lw_list_x)
#                 #print('rw',rw_list_x)
# #             for num_list in lw_list_x_num.argsort():
# #                 nn =0 
# #                 print('nm', num_list)
# #                 print(rw_list_x[0])
# #                 rw_list_x[nn] = rw_list_x[num_list]
# #                 nn = nn + 1
# #             print('ddd',rw_list_x)


# In[10]:


#             if imgFile == aa :     
#                 img = cv.imread(f"{imagePath}/{aa}")
#                 for i3 in json_data[i]['Label']['objects']:
                    
                    
#                     if i3['title'] == 'Left wing':
#                         lw_x = i3['point']['x']
#                         lw_y = i3['point']['y']  
                        
#                         lwx_1 = lw_x - bbox_g
#                         lwy_1 = lw_y + bbox_g
#                         lwx_2 = lw_x + bbox_g
#                         lwy_2 = lw_y - bbox_g
                        
#                         #print(len(lw_list_x))
#                         q = len(lw_list_x)
                        
#                         if q > 0:
#                             #print(lw_list_x[-1])
#                             if(lw_list_x[-1]<lw_x):
#                                 lw_list_x.append(lw_x); 
#                                 a_list_x1.append(lwx_1); a_list_y1.append(lwy_2); a_list_x2.append(lwx_2); a_list_y2.append(lwy_1)
#                             else:
#                                 if(lw_list_x[0]<lw_x):
#                                     lw_list_x.insert(1, lw_x); 
#                                     a_list_x1.insert(1, lwy_1); a_list_y1.insert(1, lwx_2); a_list_x2.insert(1, lwy_2); a_list_y2.insert(1, lwx_1)
                                    
#                                 else:
#                                     lw_list_x.insert(0, lw_x); 
#                                     a_list_x1.insert(0, lwy_1); a_list_y1.insert(0, lwx_2); a_list_x2.insert(0, lwy_2); a_list_y2.insert(0, lwx_1)

#                         else:
#                             lw_list_x.append(lw_x); 
#                             a_list_x1.append(lwx_1); a_list_y1.append(lwy_2); a_list_x2.append(lwx_2); a_list_y2.append(lwy_1)

#                         #print(lw_list_x)
                        

                        
     
    
#                     if i3['title']  == 'Left gear':
#                         lg_x = i3['point']['x']
#                         lg_y = i3['point']['y']
#                         lg_list_x.append(lg_x)
#                         lg_list_y.append(lg_y)


#                         lgx_1 = lg_x - bbox_g
#                         lgy_1 = lg_y + bbox_g
#                         lgx_2 = lg_x + bbox_g
#                         lgy_2 = lg_y - bbox_g
#                         a_list_x1.append(lgx_1)
#                         a_list_y1.append(lgy_2)
#                         a_list_x2.append(lgx_2)
#                         a_list_y2.append(lgy_1)
   
                        
                        
                        

#                     if i3['title']  == 'Right wing':
#                         rw_x = i3['point']['x']
#                         rw_y = i3['point']['y']
#                         rw_list_x.append(rw_x)
#                         rw_list_y.append(rw_y)

#                         rwx_1 = rw_x - bbox_g
#                         rwy_1 = rw_y + bbox_g
#                         rwx_2 = rw_x + bbox_g
#                         rwy_2 = rw_y - bbox_g
#                         a_list_x1.append(rwx_1)
#                         a_list_y1.append(rwy_2)
#                         a_list_x2.append(rwx_2)
#                         a_list_y2.append(rwy_1)
                        
                        
#                     if i3['title']  == 'Right gear':
#                         rg_x = i3['point']['x']
#                         rg_y = i3['point']['y']
#                         rg_list_x.append(rg_x)
#                         rg_list_y.append(rg_y)

#                         rgx_1 = rg_x - bbox_g
#                         rgy_1 = rg_y + bbox_g
#                         rgx_2 = rg_x + bbox_g
#                         rgy_2 = rg_y - bbox_g
#                         a_list_x1.append(rgx_1)
#                         a_list_y1.append(rgy_2)
#                         a_list_x2.append(rgx_2)
#                         a_list_y2.append(rgy_1) 
                        
                        
#                     if i3['title']  == 'Propeller':
#                         ct_x = i3['point']['x']
#                         ct_y = i3['point']['y']
#                         ct_list_x.append(ct_x)
#                         ct_list_y.append(ct_y)
                        
#                         ctx_1 = ct_x - bbox_g
#                         cty_1 = ct_y + bbox_g
#                         ctx_2 = ct_x + bbox_g
#                         cty_2 = ct_y - bbox_g
#                         a_list_x1.append(ctx_1)
#                         a_list_y1.append(cty_2)
#                         a_list_x2.append(ctx_2)
#                         a_list_y2.append(cty_1)
                        

         
#                 print(len(a_list_x1))
                     
#                 cnt = 0
#                 print(imgFile)
#                 print(aa)
                    

#                 #print(lw_list_x, lw_list_y)
#                 #print(lg_list_x, lg_list_y)
#                 #print(rw_list_x, rw_list_y)
#                 #print(rg_list_x, rg_list_y)
#                 #print(ct_list_x, ct_list_y)
                
#                 #print("-",a_list_x1,"\n-", a_list_x2) 
#                 #print("-",a_list_y1,"\n-", a_list_y2)
                             
#                 #print(land)
                

#                 for i1 in range(len(lw_list_x)):
#                     jump = len(lw_list_x)
                    
#                     #왼쪽 날개
                    
                        
#                     #cv.circle(img,(int(lw_list_x[i1]), int(lw_list_y[i1])), 1,(255,0,0))
                    
#                     if int(a_list_x1[i1]) <= int(land[i1][0]) <= int(a_list_x2[i1]):
#                         if int(a_list_y1[i1]) <= int(land[i1][1]) <= int(a_list_y2[i1]):
#                             print(int(a_list_x1[i1]),"(",int(land[i1][0]),")",int(a_list_x2[i1]),",",int(a_list_y1[i1]),"(",int(land[i1][1]),")",int(a_list_y2[i1]),"(왼쪽 날개:T)")      
#                         else:
#                             print(int(a_list_x1[i1]),"(",int(land[i1][0]),")",int(a_list_x2[i1]),",",int(a_list_y1[i1]),"(",int(land[i1][1]),")",int(a_list_y2[i1]),"(왼쪽 날개:F)")                        
#                     else:
#                             print(int(a_list_x1[i1]),"(",int(land[i1][0]),")",int(a_list_x2[i1]),",",int(a_list_y1[i1]),"(",int(land[i1][1]),")",int(a_list_y2[i1]),"(왼쪽 날개:F)")                        
                        
#                     #오른쪽 날개
#                     cv.circle(img,(int(rw_list_x[i1]), int(rw_list_y[i1])), 1,(255,0,0))
#                     if int(a_list_x1[i1+jump]) <= int(land[i1][4]) <= int(a_list_x2[i1+jump]):
#                         if int(a_list_y1[i1+jump]) <= int(land[i1][5]) <= int(a_list_y2[i1+jump]):
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][4]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][5]),")",int(a_list_y2[i1+jump]),"(오른쪽 날개:T)")                        
#                         else:
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][4]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][5]),")",int(a_list_y2[i1+jump]),"(오른쪽 날개:F)")                        
#                     else:
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][4]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][5]),")",int(a_list_y2[i1+jump]),"(오른쪽 날개:F)")                        
  
#                     #중심
#                     jump = jump + len(lw_list_x)
#                     cv.circle(img,(int(ct_list_x[i1]), int(ct_list_y[i1])), 1,(255,0,0))
#                     if int(a_list_x1[i1+jump]) <= int(land[i1][8]) <= int(a_list_x2[i1+jump]):
#                         if int(a_list_y1[i1+jump]) <= int(land[i1][9]) <= int(a_list_y2[i1+jump]):
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][8]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][9]),")",int(a_list_y2[i1+jump]),"(중심:T)")                        
#                         else:
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][8]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][9]),")",int(a_list_y2[i1+jump]),"(중심:F)")                        
#                     else:
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][8]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][9]),")",int(a_list_y2[i1+jump]),"(중심:F)")                        
  
#                     #왼쪽 바퀴
#                     jump = jump + len(lw_list_x)
#                     cv.circle(img,(int(lg_list_x[i1]), int(lg_list_y[i1])), 1,(255,0,0))
#                     if int(a_list_x1[i1+jump]) <= int(land[i1][2]) <= int(a_list_x2[i1+jump]):
#                         if int(a_list_y1[i1+jump]) <= int(land[i1][3]) <= int(a_list_y2[i1+jump]):
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][2]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][3]),")",int(a_list_y2[i1+jump]),"(왼쪽 바퀴:T)")                        
#                         else:
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][2]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][3]),")",int(a_list_y2[i1]+jump),"(왼쪽 바퀴:F)")                        
#                     else:
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][2]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][3]),")",int(a_list_y2[i1]+jump),"(왼쪽 바퀴:F)")                        
                      
                    
#                     #오른쪽 바퀴
#                     jump = jump + len(lw_list_x)
#                     cv.circle(img,(int(rg_list_x[i1]), int(rg_list_y[i1])), 1,(255,0,0))
#                     if int(a_list_x1[i1+jump]) <= int(land[i1][6]) <= int(a_list_x2[i1+jump]):
#                         if int(a_list_y1[i1+jump]) <= int(land[i1][7]) <= int(a_list_y2[i1+jump]):
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][6]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][7]),")",int(a_list_y2[i1+jump]),"(오른쪽 바퀴:T)")                        
#                         else:
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][6]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][7]),")",int(a_list_y2[i1+jump]),"(오른쪽 바퀴:F)")                        
#                     else:
#                             print(int(a_list_x1[i1+jump]),"(",int(land[i1][6]),")",int(a_list_x2[i1+jump]),",",int(a_list_y1[i1+jump]),"(",int(land[i1][7]),")",int(a_list_y2[i1+jump]),"(오른쪽 바퀴:F)")                        
  


#                 per = (cnt/5)*100
#                 print(per,"%")

#                 if(per >= 80):
#                     total = total + 1


#                 #if len(lw_list_x) == len(rw_list_x) == len(ct_list_x)==len(lg_list_x) ==len(rg_list_x):
#                 #    for ii in range(0, len(lw_list_x)):
#                 #        cv.crcle(img,(int(lw_list_x[ii]), int(lw_list_y[ii])), 1,(255,0,0))
#                 #        cv.circle(img,(int(rw_list_x[ii]), int(rw_list_y[ii])), 1,(255,0,0))
#                 #        cv.circle(img,(int(ct_list_x[ii]), int(ct_list_y[ii])), 1,(255,0,0))
#                 #        cv.circle(img,(int(lg_list_x[ii]), int(lg_list_y[ii])), 1,(255,0,0))
#                 #        cv.circle(img,(int(rg_list_x[ii]), int(rg_list_y[ii])), 1,(255,0,0))
                    
#                 #for q in range(len(img_label)):
            
                    
                    
#                 #    for p in range(0, len(img_label[q])):
                        
                        
#                 #        cv.circle(img, (int(land[p][0]), int(land[p][1])), 1, (0,0,255))
#                 #        cv.circle(img, (int(land[p][2]), int(land[p][3])), 1, (0,0,255))
#                 #        cv.circle(img, (int(land[p][4]), int(land[p][5])), 1, (0,0,255))
#                 #        cv.circle(img, (int(land[p][6]), int(land[p][7])), 1, (0,0,255))
#                 #        cv.circle(img, (int(land[p][8]), int(land[p][9])), 1, (0,0,255))
                        
                        

#                 #for iii in range(0, len(cnt)):
#                 #    cv.rectangle(img, (int(a_list_x1[iii]), int(a_list_y1[iii])), (int(a_list_x2[iii]), int(a_list_y2[iii])), rectcolor, 1)

                




#                 #img = cv.cvtColor(img, cv.COLOR_RGB2BGR)
#                 #cv.imwrite(f"{savePath}/{aa}", img)

#                 #img = mpimg.imread(f"{savePath}/{labelfile}.png")

#                 #plt.axis("off")
#                 #plt.figure(figsize=(40,40))
#                 #plt.imshow(img)
#                 #plt.show()

# print(total)


# In[ ]:





# In[ ]:




