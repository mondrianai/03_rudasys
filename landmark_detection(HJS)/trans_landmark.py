#trans_landmark.py
import os

path = "/home/ubuntu/HJS/train7"
file_list = os.listdir(path)
file_list_img = [file for file in file_list if file.endswith(".jpg")]

# print ("file_list_img: {}".format(file_list_img))

file_id = []
for i in file_list_img:
     file_id.append(i.replace('.jpg',''))
# print(file_id)

with open('/home/ubuntu/HJS/train7.txt', 'w') as f:
    import json
    for i2 in file_id:
        file_path ="/home/ubuntu/HJS/train7/{}.json".format(i2)
        with open(file_path, "r") as json_file:
            json_data = json.load(json_file)

        image_path = '/home/ubuntu/HJS/train7/' + i2 + '.jpg'


        for i3 in json_data['shapes']:
            
            if i3['label'] == 'airplane':
                
                i3['points'][2] , i3['points'][3] = i3['points'][2] - i3['points'][0] , i3['points'][3] - i3['points'][1]
                
                mylist = sum(i3['points'], [])
                
                air_points = " ".join(map(str, mylist))
        
                
            if i3['label'] == 'left_wing':
                
                mylist2 = sum(i3['points'],[])
                
                left_points = " ".join(map(str, mylist2 ))
                
            if i3['label'] == 'left_wing2':
                
                mylist3 = sum(i3['points'],[])
                
                left_points2 = " ".join(map(str, mylist3 ))
            
                
            if i3['label'] == 'right_wing':
                
                mylist4 = sum(i3['points'],[])
                
                right_points = " ".join(map(str, mylist4 ))
            
                
            if i3['label'] == 'right_wing2':
                
                mylist5 = sum(i3['points'],[])
                
                right_points2 = " ".join(map(str, mylist5 ))
                
            if i3['label'] =='center':
                
                mylist6 = sum(i3['points'],[])
                
                center_points = " ".join(map(str, mylist6))
                
        all = image_path + ' ' + air_points + ' ' + left_points + ' ' + left_points2+ ' ' + right_points + ' ' + right_points2+ ' ' + center_points
#         print(all)
        f.write(all+"\n")