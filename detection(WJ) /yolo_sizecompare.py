import torch
import matplotlib.pyplot as plt
import numpy
import pandas as pd

model = torch.hub.load('ultralytics/yolov5', 'yolov5x')  # or yolov5m, yolov5x, custom

focal_length = 36 #카메라마다 다름, focal_length
camera_between = 300 #카메라 간의 사이, distance between camera

result = pd.DataFrame() #좌표출력용 데이터프레임
distanceResult = pd.DataFrame(columns=['distance', 'disparity']) #거리결과용 데이터프레임

indexList = [] #데이터프레임 인덱스에 이미지파일명 넣으려고 만듬

#yolo로 오브젝트 세그멘테이션 진행 후에 그 이미지를 저장하는 함수
def yoloSave(): 
    for i in range(57): #캡쳐된 이미지 수만큼만 입력
        num = str(i)
        imgname = '002_' + num + '.png' #이미지 이름
        route = 'autoCapture/RWY33/002/' + imgname #이미지 경로
        #print(route)
        results = model(route) #yolo모델에 이미지 입력

        results.save('autoCapture/RWY33/002_yolo/') #저장경로


def distance(): #거리측정 함수
    for i in range(500, 562): #캡쳐된 이미지 수만큼 입력 -> 이 파일에서는 측정되는 비행기가 10~57초까지 측정됨
        num = str(i)
        imagename = 'D02_' + num + '.png' #이미지 이름

        routeRight = 'autoCapture/D02/Rch_gray/' + imagename #이미지 경로
        routeLeft = 'autoCapture/D02/Lch_Gray/' + imagename #이미지 경로

        right = model(routeRight) #yolo 모델에 이미지 입력
        left = model(routeLeft) #yolo모델에 이미지 입력

        if right.pandas().xyxy[0]['name'].size > 0 and left.pandas().xyxy[0]['name'].size > 0: #좌우이미지에서 세그멘테이션에 객체가 감지됬을 경우에만 
            if right.pandas().xyxy[0]['name'].values[0] == 'airplane' and left.pandas().xyxy[0]['name'].values[0] == 'airplane':
                #좌우 이미지에서 첫번째 감지로 비행기가 감지되었을 때만
                #yolo같은 경우에는 이미지에서 감지된 객체를 저장할 때 정확도가 높은 순서대로 저장하기 때문에 
                #비행기가 제일 정확도가 높은 객체가 비행기일때만 거리측정을 진행합니다
                
                print(imagename)
                #print(right.pandas().xyxy[0]) #오른쪽이미지 감지된 좌표출력
                #print(left.pandas().xyxy[0]) #왼쪽이미지 감지된 좌표출력
                #leftPoint = [(left.xyxy[0][0][2] + left.xyxy[0][0][0])/2, left.xyxy[0][0][1]]
                #righttPoint = [(right.xyxy[0][0][2] + right.xyxy[0][0][0])/2, right.xyxy[0][0][1]]

                #감지된 좌표에서 크기계산
                leftSize = left.xyxy[0][0][2] - left.xyxy[0][0][0]
                rightSize = right.xyxy[0][0][2] - right.xyxy[0][0][0]

                imageSize = (rightSize + leftSize) / 2
                imageSize = imageSize.cpu().detach().numpy() #cuda to float

                realSize = 6000 #실제크기

                print('이미지에서 비행기 크기 : ', imageSize) #이미지 차이 출력
                distance = realSize / imageSize * 12 #거리측정 공식
                distance = round(distance) #반올림
                print('비행기거리 : ', distance, 'm') #거리출력
                
                global result, distanceResult #전역변수 선언
                #result = result.append(right.pandas().xyxy[0]) #좌표 데이터프레임에 결과추가
                distanceResult = distanceResult.append(pd.DataFrame({'distance': distance, 'imageSize':imageSize}, index=[0])) #거리 데이터프레임에 결과추가

                indexList.append(imagename) #이미지파일이름 인덱스 추가

    #result.index = indexList #인덱스추가
    distanceResult.index = indexList #인덱스 추가

    distanceResult.to_csv('SizeDistanceResultgray.csv') #csv파일로저장
    #result.to_csv('sample.csv') #csv 파일로 저장
    #result.to_json('sample.json') #json 도 가능

#yoloSave()
distance()