import cv2 #openCV

vidcap = cv2.VideoCapture('video/1107.mp4') # 영상 경로 
count = 0 

while(vidcap.isOpened()): 
    ret, image = vidcap.read() 
    # 30프레임당 하나씩 이미지 추출

    #image = image[0:770, 800:2560] #이미지 자르기
    #image = cv2.resize(image, None, fx=1.2, fy=1.2)
    #image = cv2.fastNlMeansDenoisingColored(image,None,10,10,7,21)
    #image = cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE) #이미지 회전

    # 30프레임당 하나씩 이미지 추출
    if(int(vidcap.get(1)) % 30 == 0): 
        print('Saved frame number : ' + str(int(vidcap.get(1)))) 
        image = image[0:750, 800:2560] #이미지 자르기
        image = cv2.resize(image, None, fx=1.0, fy=1.0, interpolation=cv2.INTER_LINEAR)
        #image = cv2.fastNlMeansDenoisingColored(image,None,10,10,7,21)
        # 추출된 이미지가 저장되는 경로 
        cv2.imwrite("autoCapture/1107/1107_%d.png" % count, image) 
        print('Saved frame%d.jpg' % count) 
        count += 1 
        
vidcap.release()

