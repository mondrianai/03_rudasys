import torch
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import cv2

model = torch.hub.load('ultralytics/yolov5', 'yolov5x')  # or yolov5m, yolov5x, custom

focal_length = 36 #카메라마다 다름, focal_length
camera_between = 1000 #카메라 간의 사이, distance between camera

result = pd.DataFrame() #좌표출력용 데이터프레임
distanceResult = pd.DataFrame(columns=['distance', 'disparity']) #거리결과용 데이터프레임

indexList = [] #데이터프레임 인덱스에 이미지파일명 넣으려고 만듬




#yolo로 오브젝트 세그멘테이션 진행 후에 그 이미지를 저장하는 함수
def yoloSave(): 
    for i in range(1334): #캡쳐된 이미지 수만큼만 입력
        num = str(i)
        imgname = '33/' + num + '.jpg' #이미지 이름
        route = 'result/' + imgname #이미지 경로
        #route = 'unshark_129.png' #이미지 경로

        results = model(route) #yolo모델에 이미지 입력

        results.save('reusult/') #저장경로
        #results.save('yolo')



def distance(): #거리측정 함수
    for i in range(505,562): #캡쳐된 이미지 수만큼 입력 -> 이 파일에서는 측정되는 비행기가 10~57초까지 측정됨
        num = str(i)
        imagename = 'D02_' + num + '.jpg' #이미지 이름

        routeRight = 'Rch/' + imagename #이미지 경로
        routeLeft = 'Lch/' + imagename #이미지 경로

        right = model(routeRight) #yolo 모델에 이미지 입력
        left = model(routeLeft) #yolo모델에 이미지 입력

        if right.pandas().xyxy[0]['name'].size > 0 and left.pandas().xyxy[0]['name'].size: #좌우이미지에서 세그멘테이션에 객체가 감지됬을 경우에만 
            if right.pandas().xyxy[0]['name'].values[0] == 'airplane' and left.pandas().xyxy[0]['name'].values[0] == 'airplane':
                #좌우 이미지에서 첫번째 감지로 비행기가 감지되었을 때만
                #yolo같은 경우에는 이미지에서 감지된 객체를 저장할 때 정확도가 높은 순서대로 저장하기 때문에 
                #비행기가 제일 정확도가 높은 객체가 비행기일때만 거리측정을 진행합니다
                
                print(imagename)
                #print(right.pandas().xyxy[0]) #오른쪽이미지 감지된 좌표출력
                #print(left.pandas().xyxy[0]) #왼쪽이미지 감지된 좌표출력
                #leftPoint = [(left.xyxy[0][0][2] + left.xyxy[0][0][0])/2, left.xyxy[0][0][1]]
                #righttPoint = [(right.xyxy[0][0][2] + right.xyxy[0][0][0])/2, right.xyxy[0][0][1]]

                #감지된 좌표에서 중앙값 찾기
                leftPoint = [(left.xyxy[0][0][2] + left.xyxy[0][0][0])/2, (left.xyxy[0][0][1] + left.xyxy[0][0][3])/2] 
                righttPoint = [(right.xyxy[0][0][2] + right.xyxy[0][0][0])/2, (right.xyxy[0][0][1] + right.xyxy[0][0][3])/2]

                #피라고라스 정리 a^2+b^2=c^2 x좌표 차이, y좌표차이 
                xSub = leftPoint[0] - righttPoint[0]
                ySub = leftPoint[1] - righttPoint[1]

                disparity = (xSub*xSub)+(ySub*ySub) #c^2
                disparity = disparity ** 0.5 #제곱 지우려고 루트
                disparity = disparity.cpu().detach().numpy() #cuda to float

                #disparity = disparity * 5 #시차 차이를 많이 내보려고 쓴 변수 평소엔 사용X
                print('두 이미지 간의 차이', disparity) #이미지 차이 출력
                distance = (focal_length*camera_between/disparity) #거리공식 대입
                distance = round(distance) #반올림
                print('비행기거리 : ', distance, 'm') #거리출력
                
                global result, distanceResult #전역변수 선언
                #result = result.append(right.pandas().xyxy[0]) #좌표 데이터프레임에 결과추가
                distanceResult = distanceResult.append(pd.DataFrame({'distance': distance, 'disparity':disparity}, index=[0])) #거리 데이터프레임에 결과추가

                indexList.append(imagename) #이미지파일이름 인덱스 추가

    #result.index = indexList #인덱스추가
    distanceResult.index = indexList #인덱스 추가
    distanceResult.index = np.arange(0,len(indexList))
    distanceResult.to_csv('distanceResultbila.csv') #csv파일로저장
    #result.to_csv('sample.csv') #csv 파일로 저장
    #result.to_json('sample.json') #json 도 가능



yoloSave()
#distance()